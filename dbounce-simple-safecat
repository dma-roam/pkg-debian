#!/bin/sh
#
# Copyright (c) 2009, 2010  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e

usage()
{
	cat <<EOUSAGE
Usage:	dbounce-simple-safecat [-a addr] [-d dir] [-i queueid] [-t type] -f msg
	-a	the recipient of the bounced message
	-d	the bounce directory, default $DEFBOUNCEDIR
	-f	the full path to the bounced message to process
	-i	the ID of the bounce message in the local MTA's queue
	-t	the MTA type, currently ignored
EOUSAGE
}

DEFBOUNCEDIR='/var/spool/mail/dma-bounces'
BOUNCEDIR="$DEFBOUNCEDIR"

while getopts 'a:d:f:i:t:' o; do
	case "$o" in
		a)
			ADDR="$OPTARG"
			;;
		d)
			BOUNCEDIR="$OPTARG"
			;;
		f)
			FILENAME="$OPTARG"
			;;
		i)
			QUEUEID="$OPTARG"
			;;
		t)
			MTA="$OPTARG"
			;;
		*)
			usage 1>&2
			exit 1
	esac
done

if [ ! -r "$FILENAME" ]; then
	echo "Nonexistent or unreadable bounce file $FILENAME" 1>&2
	exit 1
fi

bname=`basename "$FILENAME"`
dname=`dirname "$FILENAME"`
if expr "$bname" : 'Q' > /dev/null; then
	qname="M${bname#Q}"
	qfile="$dname/$qname"
	if [ ! -r "$qfile" ]; then
		echo "Nonexistent or unreadable bounce queue file $qfile" 1>&2
		exit 1
	fi
else
	qfile=''
fi

F=`cat "$FILENAME" ${qfile:+"$qfile"} | safecat "$BOUNCEDIR/tmp" "$BOUNCEDIR/new"`
OUTFILE="$BOUNCEDIR/new/$F"
if [ ! -f "$OUTFILE" ]; then
	echo "safecat indicated success, but '$OUTFILE' does not exist" 1>&2
	exit 1
fi
echo "Successfully written the message to $OUTFILE"
